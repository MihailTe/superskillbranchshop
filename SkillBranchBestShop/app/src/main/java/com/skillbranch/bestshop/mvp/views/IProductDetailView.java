package com.skillbranch.bestshop.mvp.views;

import com.skillbranch.bestshop.data.storage.realm.ProductRealm;

public interface IProductDetailView extends IView{
    void initView(ProductRealm productRealm);
}
