package com.skillbranch.bestshop.di.components;

import javax.inject.Singleton;

import dagger.Component;
import com.skillbranch.bestshop.di.modules.ModelModule;
import com.skillbranch.bestshop.mvp.models.AbstractModel;

@Component(modules = ModelModule.class)
@Singleton
public interface ModelComponent {
    void inject(AbstractModel abstractModel);
}
